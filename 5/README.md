# Лекция от 07.02.2018

## Материалы

```
https://learn.javascript.ru/events-and-interfaces
```

## Задания
```
<https://learn.javascript.ru/task/carousel>
<https://learn.javascript.ru/task/move-ball-field>
<https://learn.javascript.ru/task/sliding-tree>
<https://learn.javascript.ru/task/behavior-tooltip>
<https://learn.javascript.ru/task/image-gallery>
```