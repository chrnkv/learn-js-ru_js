# Лекция от 07.16.2018

## Материалы

```
Дорешиваем 
<https://learn.javascript.ru/decorators>

Читаем 
<https://learn.javascript.ru/settimeout-setinterval>

Сложные задачи 
<https://learn.javascript.ru/task/debounce>
<https://learn.javascript.ru/task/throttle>
<http://plnkr.co/edit/L0Bsg5U0w29JAxBy5riD?p=preview>

Для знакомства с ООП <https://learn.javascript.ru/oop> главы 1 и 2
Повторяем
<https://learn.javascript.ru/prototype>
<https://learn.javascript.ru/constructor-new>
```