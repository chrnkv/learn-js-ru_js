'use strict'

class Voter {
    constructor(options) {
        this.elem = options.elem;
        this.counter = document.querySelector('.vote');
        this.onClick();
    }

    onClick() {
        let self = this;
        document.addEventListener('click', (ev) => {

            let counterBtn = ev.target.closest('.js-direction');

            if (!counterBtn) {
                return false;
            }

            let actionType = counterBtn.dataset.dir;

            switch(actionType) {
                case 'up':
                    self._countUp();
                    break;
                case 'down':
                    self._countDown();
                    break;
            }
        })
    }

    _countDown() {
        this.counter.innerHTML = +this.counter.innerHTML - 1;
    }
    _countUp() {
        this.counter.innerHTML = +this.counter.innerHTML + 1;
    }

    setVoter(number) {
        this.counter.innerHTML = number;
    }
}

let voter = new Voter({
   elem: document.querySelector('#voter')
});