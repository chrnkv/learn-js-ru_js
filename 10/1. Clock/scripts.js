'use strict'

class Clock {
    constructor(options) {
        this.elem = options.elem;
        this.isActive = false;
        this.timer = null;
        this._bindEvents();
    }

    _bindEvents() {
        let self = this;
        document.querySelector('.js-start').addEventListener('click', () => self.start());
        document.querySelector('.js-stop').addEventListener('click', () => self.stop());
    }

    _render() {
        let date = new Date(),
            dateItems = [
                {
                    name: 'hours',
                    value: date.getHours()
                },
                {
                    name: 'minutes',
                    value: date.getMinutes()
                },
                {
                    name: 'seconds',
                    value: date.getSeconds()
                }
            ];

        dateItems.forEach((item) => {
            let selector = document.querySelector(`.js-${item.name}`),
                value = +item.value,
                newValue = value > 10 ? value : `0${value}`;

            selector.innerHTML = newValue;
        })
    }

    start() {
        if (this.isActive) {
            return;
        }
        this.timer = setInterval(this._render, 1000);
        this.isActive = true;
    }

    stop() {
        clearInterval(this.timer);
        this.isActive = false;
    }
}

let clock = new Clock({
   elem: document.querySelector('.js-clock')
});