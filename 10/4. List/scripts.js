'use strict'

class listSelect {
    constructor(options) {
        this.elem = options.elem;
        this.lastElement = false;
        this.onClick();
    }

    onClick() {
        let self = this;
        document.addEventListener('click', (ev) => {
           let listElement = ev.target.closest('li');

           if (!listElement) {
               return;
           }

           let ctrlPressed = ev.metaKey || ev.ctrlKey,
               shiftPressed = ev.shiftKey;

           if (ctrlPressed) {
               self._toggleSelect(listElement);
           } else if (shiftPressed) {
               self._massSelect(listElement)
           } else {
               self._clearSelected(listElement);
               self._toggleSelect(listElement);
           }

           self.lastElement = listElement;
        });
    }

    _clearSelected(target) {
        let activeElements = document.querySelectorAll('.selected');

        if (activeElements.length) {
            activeElements.forEach((el) => {
                if (el === target) {
                    return;
                }

                el.classList.remove('selected')
            })
        }
    }

    _toggleSelect(target) {
        target.classList.toggle('selected');
    }

    _massSelect(target) {
        let from = this.lastElement || this.elem.children[0],
            noShiftPositon = target === from;

        if (noShiftPositon) {
            return;
        }

        let shiftDirection = from.compareDocumentPosition(target),
            isClickedElementBefore = shiftDirection === 4;

        if (isClickedElementBefore) {
            for (let el = from; el != target; el = el.nextElementSibling) {
                console.log(el)
                el.classList.add('selected')
            }
        } else {
            for (let el = from; el != target; el = el.previousElementSibling) {
                el.classList.add('selected')
            }
        }

        target.classList.add('selected');

    }

}

let list = new listSelect({
   elem: document.querySelector('ul')
});