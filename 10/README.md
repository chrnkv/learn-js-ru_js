# Лекция от 07.23.2018

## Задания

```
Порешать
1. https://learn.javascript.ru/task/clock
2. https://learn.javascript.ru/task/slider-widget
3. https://learn.javascript.ru/task/voter
4. https://learn.javascript.ru/task/selectable-list-component
```

##Материалы

```
Изучаем https://learn.javascript.ru/widgets
Главы 1-5

https://learn.javascript.ru/dispatch-events

Почитать про git
https://git-scm.com/book/ru/v2
Введение
https://learn.javascript.ru/screencast/git

По желанию посмотреть 
https://github.com/mgrinko/phone-catalogue-static
```
