# Лекция от 07.19.2018

## Материалы

```
Для знакомства с ООП <https://learn.javascript.ru/oop> главы 1 и 2

читаем
<https://learn.javascript.ru/prototypes>
 главы 1-6
<https://learn.javascript.ru/static-properties-and-methods>

В следующий раз <https://learn.javascript.ru/widgets> главы 1-3
<https://learn.javascript.ru/es-class>
```

### Задачи

```
<https://learn.javascript.ru/task/clock-class>
<https://learn.javascript.ru/task/clock-class-extended>
<https://learn.javascript.ru/task/menu-timer-animated>
```

### Решения

```
http://plnkr.co/edit/Lx4vaqNF5aLhfErLs33R
http://plnkr.co/edit/yylBHhtPHo5JO4knVXVY
http://plnkr.co/edit/eRyJD2ZS0IP1Qd1F107P
```